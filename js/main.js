let game;
let states = {};
const LP = [[8, -14], [8, -22], [8, -30]];

function init() {
	const config = {
		width: 800,
		height: 600,
		renderer: Phaser.AUTO,
		parent: 'game',
		antialias: true,
		multiTexture: true
	};
	game = new Phaser.Game(config);
  	for (let s in states) {
		game.state.add(s, states[s]);
	}
	game.state.start('preload');
}

function grid_dec(n) {
	for (let x = 0; x < grid.length; ++x)
	for (let y = 0; y < grid[x].length; ++y)
	for (let z = 0; z < grid[x][y].length; ++z) {
		grid[x][y][z] -= n;
	}
}

function mk_player(sprite, input) {
	sprite.anchor.setTo(0, 0);
	sprite.animations.add('idle', [0,0,1,2,2,1], 30, true);
	sprite.play('idle')
	// up, down, left & right
	sprite.ctrl = input;

	sprite.grid_pos = [4, 4];
	layer[5].add(sprite);
	sprite.hold = null;
	sprite.put_key = false;
	sprite.mv = function (dx, dy) {
		let [x, y] = this.grid_pos;
		let _x = x + dx;
		let _y = y + dy;
		if ((_x < 0 || _x >= WIDTH) || (_y < 0 || _y >= HEIGHT)) return;
		if (grid[_x][_y].length === 0) {
			this.grid_pos[0] = _x;
			this.grid_pos[1] = _y;
			if (dy !== 0) {
				layer[_y+1].add(this);
				if (this.hold !== null) layer[_y+1].add(boxes[this.hold]);
			}
		} else if (this.hold === null) {
			this.hold = take_box(_x, _y);
			let n = this.hold;
			layer[y+1].add(boxes[n]);
			layer[y+1].bringToTop(boxes[n]);
			boxes[n].children[0].play('0');
			for (let i = 1; i <= 3; ++i) {
				boxes[n].children[i].position.setTo(LP[0][0], LP[0][1]);
			}
		}
	};
	sprite.put = function (dir) {
		if (this.hold === null) return;
		let done = false;
		if (dir === 'u') {
			done = put_box(this.grid_pos[0], this.grid_pos[1]-1, this.hold);
		}
		if (dir === 'd') {
			done = put_box(this.grid_pos[0], this.grid_pos[1]+1, this.hold);
		}
		if (dir === 'l') {
			done = put_box(this.grid_pos[0]-1, this.grid_pos[1], this.hold);
		}
		if (dir === 'r') {
			done = put_box(this.grid_pos[0]+1, this.grid_pos[1], this.hold);
		}
		if (done) {
			this.hold = null;
			bump_demand();
		}
	}
}

function t_color(c) { return c === 'g' ? 0 : (c === 'r' ? 1 : 2); }
function t_abc(c) { return c.charCodeAt(0) - 'a'.charCodeAt(0); }
function t_012(c) { return c.charCodeAt(0) - '0'.charCodeAt(0); }
function box_tag() {
	const tags = boxes.map(b => { return b.name; });
	let tag = `${_.sample('rgb')}${_.sample('abcdefghijklmnopqrstuvwxyz')}`
			+ `${_.random(0,9)}`;
	while (tags.includes(tag)) {
		tag = `${_.sample('rgb')}${_.sample('abcdefghijklmnopqrstuvwxyz')}`
			+ `${_.random(0,9)}`;
	}
	return tag;
}

function mk_box(x, y, tag) {
	const group = game.add.group();
	const sprite = group.create(0, 0, 'box');
	sprite.animations.add('0', [0], 1, true);
	sprite.animations.add('1', [1], 1, true);
	sprite.animations.add('2', [2], 1, true);

	let lab = [];
	lab.push(group.create(LP[0][0], LP[0][1], 'label_bg', t_color(tag[0])));
	lab.push(group.create(LP[0][0], LP[0][1], 'label_abc', t_abc(tag[1])));
	lab.push(group.create(LP[0][0], LP[0][1], 'label_012', t_012(tag[2])));

	group.position.setTo(-TILE_SIZE, -TILE_SIZE);
	sprite.anchor.setTo(0, 1);
	lab.forEach(l => { l.anchor.setTo(0, 1); });
	group.name = tag;
	boxes.push(group);
	put_box(x, y, boxes.length - 1);
}

function put_box(x, y, num) {
	if ((x < 0 || x >= WIDTH) || (y < 0 || y >= HEIGHT)) return false;
	if (grid[x][y].length >= 3) return false;
	grid[x][y].push(num);
	draw_boxes();
	return true;
}

function take_box(x, y) {
	if ((x < 0 || x >= WIDTH) || (y < 0 || y >= HEIGHT)) return null;
	//if (grid[x][y].length === 3) return null;
	res = grid[x][y].pop();
	draw_boxes();
	return res;
}

function draw_boxes() {
	for (let i = 0; i < boxes.length; ++i) {
		boxes[i].position.setTo(-8*TILE_SIZE, -8*TILE_SIZE);
	}
	for (let x = 0; x < WIDTH; ++x)
	for (let y = 0; y < HEIGHT; ++y) {
		if (grid[x][y].length === 0) continue;
		let n = _.last(grid[x][y]);
		layer[y+1].add(boxes[n]);
		boxes[n].position.setTo(
			x*TILE_SIZE,
			(y+1)*TILE_SIZE
		);
		const h = grid[x][y].length - 1;
		boxes[n].children[0].play(`${h}`);
		for (let i = 1; i <= 3; ++i) {
			boxes[n].children[i].position.setTo(LP[h][0], LP[h][1]);
		}
	}
}

function game_over(reason) {
	fin = true;
	document.getElementById('score').innerHTML = `${score}`;
	document.getElementById('reason').innerText = reason;
	document.getElementById('game-over').classList.remove('hidden');
}

function new_game() {
	score = 0;
	for (let i = 1; i < layer.length; ++i) {
		layer[i].removeAll();
	}
	for (let i = 0; i < boxes.length; ++i) {
		boxes[i].destroy();
	}
	for (let x = 0; x < grid.length; ++x)
	for (let y = 0; y < grid.length; ++y) {
		grid[x][y] = [];
	}
	spawn_queue = [];
	spawn_delay = 3000;
	spawner.delay = spawn_delay;
	demand_queue = [];
	cooldown.in_prog = false;
	cooldown.timer = null;
	fin = false;
	next_batch();
	bump_demand();
	pl[PL1].grid_pos = [4, 4];
	layer[5].add(pl[PL1]);
	document.getElementById('game-over').classList.add('hidden');
}
