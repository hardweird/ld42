const MAX_BOXES = 16;
let spawner;
let spawn_delay = 5000;
let spawn_queue = [];
let demand_queue = [];
let spawn_pts = [[WIDTH-1, 1], [WIDTH-1, 4], [WIDTH-1, 7]];
const DEMAND_PTS = [[0, 1], [0, 4], [0, 7]];

function init_spawner() {
	next_batch();
	demand_queue = _.shuffle(_.range(MAX_BOXES));
	draw_demands();
	spawner = game.time.events.loop(spawn_delay, () => {
		if (spawn_queue.length === 0) {
			next_batch();
			if (spawn_delay > 1000) spawn_delay -= 100;
			spawner.delay = spawn_delay;
			console.log('WHOOOO!');
		}
		let spw = JSON.parse(JSON.stringify(spawn_pts));
		for (let i = 1; i < WIDTH-1; ++i) {
			spw = _.filter(spw, (l) => {
				return grid[l[0]][l[1]].length < 3
					&& !(pl[PL1].grid_pos[0] === l[0]
						&& pl[PL1].grid_pos[1] === l[1]);
			}); 
			if (spw.length === 0) {
				spw = [[WIDTH-i, 0], [WIDTH-i-1, 1], [WIDTH-i, 2], [WIDTH-i, 3],
					[WIDTH-i-1, 4], [WIDTH-i, 5], [WIDTH-i, 6], [WIDTH-i-1, 7],
					[WIDTH-i,8]]; 
			} else {
				break;
			}
		}
		let [x, y] = _.sample(spw);
		if (x === 2) {
			game.paused = true;
			game_over('What a mess!');
		}
		let n = spawn_queue.pop();
		put_box(x, y, n);

		[x, y] = pl[PL1].grid_pos;
		let u = [[x-1, y], [x+1, y], [x, y-1], [x, y+1]];
		let sum = 0;
		for (let i = 0; i < u.length; ++i) {
			[x, y] = u[i];
			if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT) {
				sum += 3;
				continue;
			}
			sum += grid[x][y].length;
		}
		if (sum >= 11 || (sum >= 10 && pl[PL1].hold !== null)) {
			game.paused = true;
			game_over("You're stuck...");
		}
	});
	//spawner.start();
}

function next_batch() {
	for (let i = 0; i < MAX_BOXES; ++i) {
		mk_box(-TILE_SIZE, -TILE_SIZE, box_tag());
	}
	spawn_queue = _.shuffle(_.range(boxes.length - MAX_BOXES, boxes.length));
}

function bump_demand() {
	let changed = false;
	for (let i = 0; i < DEMAND_PTS.length; ++i) {
		let [x, y] = DEMAND_PTS[i];
		if (grid[x][y].length === 0) continue;

		let box = _.last(grid[x][y]);
		let demand = demand_queue[i];
		if (box !== demand) {
			//TODO fire some visual
			continue;
		}
		changed = true;
		score += 5;
		grid[x][y].pop();
		demand_queue = _.first(demand_queue, i)
			.concat(_.last(demand_queue, demand_queue.length - i - 1));
	}
	if (changed) {
		draw_demands();
		draw_boxes();
	}
	console.log(_.first(demand_queue.map((d) => { return boxes[d].name; }), 3));
}

function draw_demands() {
	if (demand_queue.length === 0) {
		for (let i = 0; i < MAX_BOXES; ++i) {
			boxes[i].parent.remove(boxes[i], true);
		}
		boxes = _.last(boxes, boxes.length - MAX_BOXES);
		for (let i = 0; i < spawn_queue.length; ++i) {
			spawn_queue[i] -= MAX_BOXES;
		}
		grid_dec(MAX_BOXES);
		demand_queue = demand_queue.concat(
			_.shuffle(_.range(MAX_BOXES))
		);
	}
	let dq = _.first(demand_queue, 3) || [];
	console.log(dq.map(d => { return boxes[d].name; }));
	for (let i = 0; i < DEMAND_PTS.length; ++i) {
		if (i >= dq.length) {
			bubbles[i].visible = false;
			continue;
		}
		bubbles[i].visible = true;
		let name = boxes[dq[i]].name;
		bubbles[i].children[0].play(name[0]);
		bubbles[i].children[1].play(name[1]);
		bubbles[i].children[2].play(name[2]);
	}
}
