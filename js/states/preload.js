const TILE_SIZE = 64;
const LABEL_SIZE = 48;
const HEIGHT = 9;
const WIDTH = 12;

states['preload'] = {
	preload: () => {
		game.load.image('floor', 'img/bg.png');
		game.load.image('null', 'img/null.png');
		game.load.spritesheet('box', 'img/box.png', TILE_SIZE, TILE_SIZE+24);
		game.load.spritesheet('pl1', 'img/pl.png', TILE_SIZE, 96);
		game.load.spritesheet('label_bg', 'img/label_bg.png',
			LABEL_SIZE, LABEL_SIZE);
		game.load.spritesheet('label_abc', 'img/label_abc.png',
			LABEL_SIZE, LABEL_SIZE);
		game.load.spritesheet('label_012', 'img/label_012.png',
			LABEL_SIZE, LABEL_SIZE);
		game.load.spritesheet('bubble', 'img/bubble.png',
			72, 40);
	},
	create: () => {
		document.querySelector('canvas').oncontextmenu
			= function() { return false; };
		game.state.start('game');
	}
}
