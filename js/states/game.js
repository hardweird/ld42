const PL1 = 0;
const PL2 = 1;
const debug = [];

let score = 0;
let bg;
let pl = [];
let boxes = [];
let bubbles = [];
let grid = [];
for (let x = 0; x < WIDTH; ++x) {
	grid.push([]);
	for (let y = 0; y < HEIGHT; ++y) {
		grid[x].push([]);

	}
}
let layer = [];
const cooldown = {
	in_prog: false,
	timer: null,
	delay: 150,
	cb: () => { cooldown.in_prog = false; }
};
let pause_key;
let enter_key;
let fin = false;

states['game'] = {
	init: () => {
	},
	create: () => {
		game.world.setBounds(0, -24, 800, 624);
		game.camera.y = -24;
		pause_key = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
		enter_key = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
		// background layer
		layer.push(game.add.group());
		for (let i = 0; i < HEIGHT; ++i) {
			// layers for crates and players
			layer.push(game.add.group());
		}
		bg = layer[0].create(0, -24, 'floor');
		pl.push(game.add.sprite(160, 160, 'pl1'));
		p1_ctl = game.input.keyboard.createCursorKeys();
		p1_ctl['put'] = game.input.keyboard.addKey(Phaser.Keyboard.Z)
		mk_player(pl[PL1], p1_ctl);
		// TODO player 2
		//pl.forEach((p) => { p.bringToTop(); });
		for (let i = 0; i < DEMAND_PTS.length; ++i) {
			let [x, y] = DEMAND_PTS[i];
			let b = game.add.group();
			bubbles.push(b);
			b.position.setTo(x*TILE_SIZE, (y-1)*TILE_SIZE);
			let s0 = b.create(0, 0, 'bubble')
			s0.animations.add('g', [0], 1, true);
			s0.animations.add('r', [1], 1, true);
			s0.animations.add('b', [2], 1, true);

			let s1 = b.create(12, 0, 'label_abc')
			'abcdefghijklmnopqrstuvwxyz'.split('').forEach((e) => {
				s1.animations.add(e, [t_abc(e)], 1, true);
			});
			let s2 = b.create(12, 0, 'label_012')
			'0123456789'.split('').forEach((e) => {
				s2.animations.add(e, [t_012(e)], 1, true);
			});
		}
		init_spawner();
		game.paused = true;
	},
	update: () => {
		// TODO player 2
		
		// ====== [INPUT] ======
		if (fin) {
			if (pause_key.justPressed()) {
				// TODO new game
			}
			return;
		}
		// just move
		const dur = 100;
		if (pl[PL1].ctrl.up.isDown) {
			if (!cooldown.in_prog) {
				if (pl[PL1].put_key) pl[PL1].put('u');
				else pl[PL1].mv(0, -1);

				pl[PL1].put_key = false;
				cooldown.in_prog = true;
				cooldown.timer
					= game.time.events.add(cooldown.delay, cooldown.cb);
			}
		} else if (pl[PL1].ctrl.down.isDown) {
			if (!cooldown.in_prog) {
				if (pl[PL1].put_key) pl[PL1].put('d');
				else pl[PL1].mv(0, 1);

				pl[PL1].put_key = false;
				cooldown.in_prog = true;
				cooldown.timer
					= game.time.events.add(cooldown.delay, cooldown.cb);
			}
		} else if (pl[PL1].ctrl.right.isDown) {
			if (!cooldown.in_prog) {
				if (pl[PL1].put_key) pl[PL1].put('r');
				else pl[PL1].mv(1, 0);

				pl[PL1].put_key = false;
				cooldown.in_prog = true;
				cooldown.timer
					= game.time.events.add(cooldown.delay, cooldown.cb);
			}
		} else if (pl[PL1].ctrl.left.isDown) {
			if (!cooldown.in_prog) {
				if (pl[PL1].put_key) pl[PL1].put('l');
				else pl[PL1].mv(-1, 0);

				pl[PL1].put_key = false;
				cooldown.in_prog = true;
				cooldown.timer
					= game.time.events.add(cooldown.delay, cooldown.cb);
			}
		} else if (cooldown.timer) {
			// stop, y'know...
			cooldown.timer.callback = () => {};
			cooldown.in_prog = false;
		}
		if (pl[PL1].ctrl.put.justPressed()) {
			pl[PL1].put_key = !pl[PL1].put_key;
		}
		if (pause_key.justPressed()) {
			document.getElementById('pause').classList.remove('hidden');
			game.paused = true;
		}

		pl[PL1].position.setTo(
			pl[PL1].grid_pos[0]*TILE_SIZE,
			(pl[PL1].grid_pos[1] - .5)*TILE_SIZE
		);
		if (pl[PL1].hold !== null) {
			boxes[pl[PL1].hold].position.setTo(
				pl[PL1].grid_pos[0]*TILE_SIZE,
				(pl[PL1].grid_pos[1]+.5)*TILE_SIZE
			);
		}
	},
	paused: () => {},
	pauseUpdate: () => {
		if (pause_key.justPressed() || enter_key.justPressed()) {
			document.getElementById('pause').classList.add('hidden');
			document.getElementById('start').classList.add('hidden');
			game.paused = false;
			if (fin) new_game();
		}
	},
	render: () => {
		if (debug.includes('player')) {
			game.debug.spriteInfo(pl[0], 32, 32);
			pl.forEach((p) => {
				game.debug.body(p);
				//game.debug.body(p.magnet)
			});
		}
		if (debug.includes('boxes')) {
			boxes.forEach((b) => {
				game.debug.body(b);
			});
		}
	}
}
